"""
A simple example of an animated plot
"""
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.animation as animation

def initial_condition(n,w):
    """ --------------------------------------------------------------------
        The initial condition is a density which is a Gaussian of width w
        --------------------------------------------------------------------
    """
    x=np.zeros((n,n))
    y=np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            x[i,j]=i-n/2
            y[i,j]=j-n/2
    class ic:
        rho=np.exp(-(x**2+y**2)/w**2)
        ux=1.0; uy=0.5; dx=1.0
        play=True
    return ic

def dd(f,axis):
    """ --------------------------------------------------------------------
        Derivatives evaluated with central differences
        --------------------------------------------------------------------
    """
    a=8./12.; b=-1./12.
    return (np.roll(f,-1,axis=axis)-np.roll(f,1,axis=axis))*a + \
           (np.roll(f,-2,axis=axis)-np.roll(f,2,axis=axis))*b

def time_step(f,courant):
    """ --------------------------------------------------------------------
        Simplest possible time step -- first order in time
        --------------------------------------------------------------------
    """
    umax=np.sqrt(np.max(f.ux**2+f.uy**2))
    dt=courant*f.dx/umax
    f.rho[:]=f.rho-(dt/f.dx)*(dd(f.rho*f.ux,1)+dd(f.rho*f.uy,0))
    return f

# Set number of time steps and the Courant number
n_step=300
courant=0.2

# Make an image of the initial density distribution with imshow()
fig=pl.figure()
f=initial_condition(64,10.0)
im=pl.imshow(f.rho,origin='lower')

# Gets called by FuncAnimation, takes a timestep and updates the image
def animate(i):
    time_step(f,courant)
    im.set_data(f.rho)

# Standard animations in Python -- parameters are obvious.
ani = animation.FuncAnimation(fig, animate, n_step, repeat=None, interval=20)
pl.show()
